# Token Wallet
A simple Ethereum Wallet based on ERC20 Token. Send a Receive ETH and Token with ease and security. Ethereum wallets are decrypted and raw transactions are signed locally. Raw Transactions are sent directly to EtherScan.io. 
This wallet is locked to the Token contract address: `0x91923993C4Dc3e089BBb1fc9d4A5A765A479B68f`


## Token Wallet Features
- Use Keystore JSON wallet
- Use Private Key wallet
- Send and Receive Ethereum (ETH)
- Send a Receive tokens. ([0x91923993C4Dc3e089BBb1fc9d4A5A765A479B68f](https://etherscan.io/address/0x91923993C4Dc3e089BBb1fc9d4A5A765A479B68f))
- Set custom Gas Price for transactions
- Portfolio value for Tokens and Ethereum in USD
- Copy address to clipboard when clicked
- Transaction ID with etherscan URL once transaction is submitted
- Check for Updates button (in About - bottom right)
- Raw Transactions send to EtherScan

<h3>Transactions</h3>
This Wallet will allow you to set a custom Gas Price if you need to change the price. By default it is set to *21* gwei. Minimum is 5 gwei. The gas limit on a normal Ether transaction is *12000*. The gas limit on sending Tokens is *65000*.


<h3>Source Code Auditing</h3>
Feel free to look at what this application is doing. Token Wallet was built in electron, jquery, bootstrap, and uses ether.js for decrypting wallets. Below you'll find the main functionality of the application.

### Audit Lines
- [Token Contract](https://gitlab.com/eugenekyselev/wallet-to-wallet-mechanism/tree/master/js/main.js#L22)
- [Send Ethereum Transaction](https://gitlab.com/eugenekyselev/wallet-to-wallet-mechanism/tree/master/js/main.js#L319)
- [Send Token Transaction](https://gitlab.com/eugenekyselev/wallet-to-wallet-mechanism/tree/master/js/main.js#L367)
- [Decrypting Keystore JSON](https://gitlab.com/eugenekyselev/wallet-to-wallet-mechanism/tree/master/js/main.js#L276)
- [Using Private Key](https://gitlab.com/eugenekyselev/wallet-to-wallet-mechanism/tree/master/js/main.js#L163)

### Build
You can compile this electron application on your own.
- `npm install`
- `npm start`